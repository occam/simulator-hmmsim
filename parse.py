# Generates an output.json representing the output of the simulator.

import re
import json
from occam import Occam

object = Occam.load()

f = open(object.output(), 'r')
#f = open("run-out.raw", 'r')
#f = open("trace_script_const_1000_noshared.pcm.out", 'r')
#f = open("trace_script_const_1000_noshared_and_trace_script_real_noshared.dram.out", 'r')

results = {}

for line in f:
  if line.startswith('cpu_'):
    # get cpu id
    things = line.split()
    key = things[0]
    value = things[1]

    parts = key.split('_')

    cpu_id = int(parts[1])

    if not 'cpus' in results:
      results['cpus'] = []

    while cpu_id >= len(results['cpus']):
      results['cpus'].append({})

    key = "_".join(parts[2:])

    results['cpus'][cpu_id][key] = value
  elif line.startswith('data_L1_'):
    # get cpu id
    things = line.split()
    key = things[0]
    value = things[1]

    parts = key.split('_')

    cpu_id = int(parts[2])

    if not 'cpus' in results:
      results['cpus'] = []

    while cpu_id >= len(results['cpus']):
      results['cpus'].append({})

    if not 'data_L1' in results['cpus'][cpu_id]:
      results['cpus'][cpu_id]['data_L1'] = {}

    key = "_".join(parts[3:])

    results['cpus'][cpu_id]['data_L1'][key] = value
  elif line.startswith('instr_L1_'):
    # get cpu id
    things = line.split()
    key = things[0]
    value = things[1]

    parts = key.split('_')

    cpu_id = int(parts[2])

    if not 'cpus' in results:
      results['cpus'] = []

    while cpu_id >= len(results['cpus']):
      results['cpus'].append({})

    if not 'instr_L1' in results['cpus'][cpu_id]:
      results['cpus'][cpu_id]['instr_L1'] = {}

    key = "_".join(parts[3:])

    results['cpus'][cpu_id]['instr_L1'][key] = value
  elif line.startswith('dram_bank'):
    # get cpu id
    things = line.split()
    key = things[0]
    value = things[1]

    parts = key.split('_')

    bank_id = int(parts[2])

    if not 'dram' in results:
      results['dram'] = {}
      results['dram']['banks'] = []

    while bank_id >= len(results['dram']['banks']):
      results['dram']['banks'].append({})

    key = "_".join(parts[3:])

    results['dram']['banks'][bank_id][key] = value
  elif line.startswith('dram_'):
    # get cpu id
    things = line.split()
    key = things[0]
    value = things[1]

    parts = key.split('_')

    if not 'dram' in results:
      results['dram'] = {}
      results['dram']['banks'] = []

    key = "_".join(parts[1:])

    results['dram'][key] = value
  elif line.startswith('pcm_bank'):
    # get cpu id
    things = line.split()
    key = things[0]
    value = things[1]

    parts = key.split('_')

    bank_id = int(parts[2])

    if not 'pcm' in results:
      results['pcm'] = {}
      results['pcm']['banks'] = []

    while bank_id >= len(results['pcm']['banks']):
      results['pcm']['banks'].append({})

    key = "_".join(parts[3:])

    results['pcm']['banks'][bank_id][key] = value
  elif line.startswith('pcm_'):
    # get cpu id
    things = line.split()
    key = things[0]
    value = things[1]

    parts = key.split('_')

    if not 'pcm' in results:
      results['pcm'] = {}
      results['pcm']['banks'] = []

    key = "_".join(parts[1:])

    results['pcm'][key] = value
  elif line.startswith("total_events "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("execution_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("event_rate "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_critical_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_read_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_write_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_queue_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_queue_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_total_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_total_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_read_service_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_write_service_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_total_service_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_read_service_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_write_service_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_service_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_read_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_write_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_avg_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_row_buffer_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_0_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_1_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_2_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_3_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_4_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_5_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_6_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_7_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_8_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_9_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_10_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_11_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_12_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_13_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_14_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_15_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_16_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_17_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_18_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_19_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_20_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_21_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_22_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_23_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_24_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_25_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_26_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_27_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_28_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_29_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_30_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_31_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_32_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_33_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_34_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_35_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_36_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_37_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_38_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_39_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_40_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_41_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_42_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_43_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_44_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_45_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_46_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_47_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_48_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_49_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_50_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_51_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_52_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_53_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_54_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_55_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_56_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_57_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_58_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_59_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_60_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_61_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_62_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_read_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_write_requests "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_read_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_write_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_read_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_write_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_row_buffer_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_row_buffer_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_num_opens "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_num_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_num_closes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_num_read_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_num_read_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_num_write_after_read "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_num_write_after_write "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_wait_lower_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_wait_same_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("dram_bank_63_wait_higher_priority_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_all_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_misses_without_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_misses_with_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_misses_with_writeback "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_misses_without_free_block "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_data_load_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_data_load_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_data_store_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_data_store_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_instr_load_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_instr_load_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_flushes_without_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_flushes_with_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_flushes_with_writeback "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_tag_change_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_tag_change_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_all_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_hit_rate "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_miss_rate "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_read_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_misses_from_flush "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("L2_writebacks_from_flush "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("memory_size "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("memory_size_used "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("num_processes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_all_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_misses_without_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_misses_with_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_misses_with_writeback "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_misses_without_free_block "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_data_load_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_data_load_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_data_store_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_data_store_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_instr_load_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_instr_load_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_flushes_without_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_flushes_with_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_flushes_with_writeback "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_tag_change_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_tag_change_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_all_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_hit_rate "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_miss_rate "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_read_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_misses_from_flush "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("instr_L1_0_writebacks_from_flush "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_all_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_misses_without_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_misses_with_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_misses_with_writeback "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_misses_without_free_block "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_data_load_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_data_load_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_data_store_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_data_store_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_instr_load_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_instr_load_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_flushes_without_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_flushes_with_eviction "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_flushes_with_writeback "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_tag_change_hits "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_tag_change_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_all_misses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_accesses "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_hit_rate "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_miss_rate "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_read_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_misses_from_flush "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("data_L1_0_writebacks_from_flush "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instructions_executed "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instruction_reads "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_reads "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_writes "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_sleep_cycles "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_access_cycles "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_pause_cycles "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_ipc "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L1_wait_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L2_wait_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_cpu_pause_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_cpu_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L1_tag_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L1_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L2_tag_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L2_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_bus_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_bus_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_bus_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_bus_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L1_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_L2_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_close_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_open_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_dram_access_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_close_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_open_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_instr_pcm_access_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_total_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L1_wait_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L2_wait_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_cpu_pause_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_cpu_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L1_tag_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L1_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L2_tag_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L2_stall_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_bus_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_bus_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_close_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_open_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_access_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_bus_queue_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_bus_time "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L1_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_L2_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_close_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_open_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_dram_access_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_close_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_open_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

  elif line.startswith("cpu_0_data_pcm_access_count "):
    ar = line.split()
    results[ar[0]] = ar[1]

o = open("%s/output.json" % (object.path()), "w+")
o.write(json.dumps(results))

o.close()

# Report warnings/errors to OCCAM
Occam.finish([], [])
