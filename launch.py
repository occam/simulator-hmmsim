import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
traces_path    = "%s/traces" % (scripts_path)
binary_path         = "%s/Simulator/obj-intel64/sim" % (scripts_path)
#configs_path         = "%s/configs" % (scripts_path)
job_path       = os.getcwd()

object = Occam.load()
inputs = object.inputs("trace")

data = object.configuration("HMMSim Options")

# Create configuration file
input_file = open('config','w+')

# keep track of memory organization
org = "none"

# Cycle through all options
for k, v in data.items():
  if k == "memory_organization" and v == 0:
    input_file.write("-"+k+" dram\n")
    org = "dram"
  elif k == "memory_organization" and v == 1:
    input_file.write("-"+k+" pcm\n")
    org = "pcm"
  elif k.startswith('dram_') and org == "dram":
    input_file.write("-"+k+" "+str(v)+"\n")
  elif k.startswith('pcm_') and org == "pcm":
    input_file.write("-"+k+" "+str(v)+"\n")
  elif not k == "trace":
    input_file.write("-"+k+" "+str(v)+"\n")


input_file.close()

# Paths to traces and config
trace_path = "%s/tiny_trace_0" % (traces_path)
config_path = "config"

# Form arguments
args = [binary_path,
        config_path]

if len(inputs) > 0:
  for trace in inputs:
    traces_path = trace.volume()
    trace_path = "%s/trace" % traces_path
    args.append(trace_path)

# Form command line
command = ' '.join(args)

# Form command to gather results
finish_command = "python %s/parse.py" % (scripts_path)

# Tell OCCAM how to run HMMSim
Occam.report(command, finish_command)
